**![](doodle.png)**

Jorgen D’Hondt 
_verantwoordelijke uitgever_


Sophie Dejaegher
Marianne Van Remoortel
_hoofdredactie_

Frederik De Laender Ben Dhooge
Jelle Haemers  Samuel Mareel
Pieter Martens
Karolien Poels
Katrien Remaut  Giovanni Samaey
Mathieu Vinken
_redactieraad_

Sophie Dejaegher
Ben Dhooge
Dorien Van De Mieroop
Marianne Van Remoortel 
_eindredactie_

Wouter Soudan @ Rhythmus.be
_vormgeving & zetwerk_

Drukkerij Peeters, Herent
_druk- en bindwerk_

—

Copyright © 2015 by Jonge Academie
en de respectievelijke auteurs. 
Alle rechten voorbehouden.

---

_leden_
Emiliano Acosta 
Jan Aerts 
Koenraad Brosens 
Alexander De Becker 
Goedele De Clerck
Stefanie Dedeurwaerdere 
Veerle De Herdt
Frederik De Laender 
Simon De Meyer 
Koen De Temmerman 
Michael De Volder
Karolien De Wael 
Jorgen D’Hondt 
Ben Dhooge
Ann Dooms 
Liesbet Geris 
Jelle Haemers 
Niel Hens
Steven Husson 
Tine Huyse 
Koenraad Jonckheere 
Tina Kyndt 
Sarah Lebeer
Samuel Mareel 
Lennart Martens 
Pieter Martens
Johan Meyers 
Johannes Nicaise 
Hans Op de Beeck 
Karolien Poels 
Korneel Rabaey 
Katrien Remaut 
Noël Salazar 
Giovanni Samaey 
Violet Soen 
Wim Thielemans
Peter Van Aelst 
Dorien Van De Mieroop 
Pieter Van Der Veken 
Lieve Van Hoof
Ine Van Hoyweghen 
Peter Van Nuffelen 
Marianne Van Remoortel 
Bram Vanderborght 
Kim Verbeken 
Marian Verhelst 
Nathalie Vermeulen
Mathieu Vinken
