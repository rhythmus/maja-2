TO DO MAJA 2 ONLINE
===================

- in gedrukte Maja: info: online gedrag → invloed

- lanceren op 12 maart
	> Voor alle duidelijkheid: de drukker heeft anderhalve week nodig. Ik denk dat we dus vrijdag goed voor druk moeten kunnen geven.

- ✗ op één of andere manier weten wie wat aanklikt
	> Dat hoeft dus niet meer.
	> Ik gaf je toch de tekst door die er zou moeten staan
	>> staat er

- domein → `http://maja.jongeacademie.be/`
	> Voor mij prima, maar bedenk dat het dan erg lange URLs worden (die ook naar iets complexere/brozere QR-codes vomzetten), en dat je dan tijdig/snel de sysadmin moet vastkrijgen om het subdomein op te zetten en de DNS pointer te wijzigen.
	>> Laat je me nog weten wat ik precies moet doen ivm die domeinnaam en DNS-pointer?

- “namespace” → `/2/`
- URLs ( = domein + namespace + path + slug)
```
http://maja.jongeacademie.be/2/
http://maja.jongeacademie.be/2/voorwoord
http://maja.jongeacademie.be/2/interdisciplinair
http://maja.jongeacademie.be/2/interdisciplinair/boom
http://maja.jongeacademie.be/2/academiclifeasitis
http://maja.jongeacademie.be/2/ik-was-35
http://maja.jongeacademie.be/2/ik-was-35/Veretennicoff
http://maja.jongeacademie.be/2/mobiliteit
http://maja.jongeacademie.be/2/mobiliteit/dwarskijker
http://maja.jongeacademie.be/2/mobiliteit/transnationaal
http://maja.jongeacademie.be/2/mobiliteit/waarom
http://maja.jongeacademie.be/2/inspiratie
http://maja.jongeacademie.be/2/inspiratie/Braeckman
http://maja.jongeacademie.be/2/JA
http://maja.jongeacademie.be/2/JA/kroniek
http://maja.jongeacademie.be/2/JA/standpunt
http://maja.jongeacademie.be/2/JA/nieuwe-leden
http://maja.jongeacademie.be/2/JA/voorzitterschap
http://maja.jongeacademie.be/2/weersvoorspelling
http://maja.jongeacademie.be/2/generatiekloof
http://maja.jongeacademie.be/2/faalweetjes/1
http://maja.jongeacademie.be/2/OttoWarburg
http://maja.jongeacademie.be/2/dwaalsporen
http://maja.jongeacademie.be/2/dwaalsporen/Marijn
http://maja.jongeacademie.be/2/dwaalsporen/Frederik
http://maja.jongeacademie.be/2/faalweetjes/2
http://maja.jongeacademie.be/2/zonder-resultaat
http://maja.jongeacademie.be/2/dagboek
http://maja.jongeacademie.be/2/dagboek/Liesbet
http://maja.jongeacademie.be/2/column
http://maja.jongeacademie.be/2/colofon
```

>>> … Eigenlijk zou die logica ook daar het dagboek moeten worden doorgetrokken, maar i.t.t. de column, wordt de eigenlijke dagboektekst nog voorafgegaan door een introductietekst: die “rubriek”-intro moet afzonderlijk bereikbaar zijn en krijgt dus een eigen path `/dagboek`.
>>>> Ja, moeilijke, want ik speel voor volgende editie met de idee om dagboek-fragmenten (en dus echt maar stukjes van een geheel dat verder groter is) door het hele magazine heen te zetten. Ik weet niet of dit je net wel of net niet helpt bij het dagboekprobleem.
>>>>> Dat kan dan perfect als volgt:
```
http://maja.jongeacademie.be/3/dagboek
http://maja.jongeacademie.be/3/dagboek/Miep
http://maja.jongeacademie.be/3/dagboek/Roos
http://maja.jongeacademie.be/3/dagboek/Rik
```
>>>>>> Ok, super. Laat je me nog weten wat ik precies moet doen ivm die domeinnaam en DNS-pointer?
