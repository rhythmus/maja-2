# Produce PDFs from all Markdown files in a directory
# Lincoln Mullen | http://lincolnmullen.com | lincoln@lincolnmullen.com

# List files to be made by finding all *.md files and appending .pdf
DOCX := $(patsubst %.docx,%.docx.md,$(wildcard *.docx))

# The all rule makes all the PDF files listed
all : $(DOCX)

# This generic rule accepts PDF targets with corresponding Markdown
# source, and makes them using pandoc
%.docx.md : %.docx
	pandoc $< -o $@  --no-wrap

# Remove all PDF outputs
clean :
	rm $(DOCX)

# Remove all PDF outputs then build them again
rebuild : clean all
