﻿TO DO
=====

# ✓ COVER + INHOUD
# ✓ COLOFON
# ✓ VOORWOORD
# ✓ GRILLIGE BOOM
# ✓ ACADEMICLIFEASITIS
# ✓ IRINA VERETENNICOFF

# DATAVIZ + KAART
- ✓ Rijsel eruit
- ✓ twee toptienen een regel omhoog
- ✓ top 10 landen: op 1 ‘Verenigde Staten’
	- ✓ 2 baretten, 7 huisjes, 4 bedjes, 25 vliegtuigjes
		> 2+7+4+25 = 38
	- de volledige 39 symbolen
		> 38 ≠ 39
	- 🎓🎓🏠🏠🏠🏠🏠🏠🏠🏨🏨🏨🏨✈✈✈✈✈✈
	  ✈✈✈✈✈✈✈✈✈✈✈✈✈✈✈✈✈✈✈
	- ✗ over 2 regels spreiden, 19 per regel
		> Dan is #1 korter dan #2
		> gaat in tegen elke consistentie voor bar charts
- ✓ twee huidige entries met ‘VS’ vallen weg.
- ✓ op nummer 10: Zwitserland, met 3 vliegtuigjes

# ✓ INTERNATIONALE (IM)MOBILITEIT
# ✓ TRANSNATIONALE MOBILITEIT
# ✓ MOEDER WAAROM REIZEN WIJ?
# ✓ JOHAN BRAECKMAN
# ✓ KRONIEK
# ✓ STANDPUNT
# ✓ NIEUWE LEDEN
# ✓ HET VOORZITTERSCHAP VAN DE JA
# ✓ FALENDE WEERSVOORSPELLING
# ✓ DE GENERATIEKLOOF GEDICHT
# ✓ FAALWEETJES
# ✓ BRIEFKAART
# ✓ DWAALSPOREN
# ✓ ZONDER RESULTAAT
# ✓ DAGBOEK
# ✓ COLUMN
# ✓ QR CODES

# ✓ ACHTERPLAT
- ✓ toch graag http://jongeacademie.be erbij voegen
	> toegevoegd zonder protocol (`http://`), aangezien dat instabiel is (`https://`)
	> Maja.JongeAcademie.be met tablet icoontje
