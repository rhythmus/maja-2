# De Jonge Academie gaat vreemd

Van jonge onderzoekers wordt een grote mobiliteit verwacht. Internationale contacten zijn niet voldoende: vooral een langdurig verblijf in het buitenland wordt verondersteld het onderzoek en de onderzoeker een boost te geven. Maar wat verwachten jonge onderzoekers zelf? Waarom reizen ze, waar gaan ze heen, en wat is hun visie op het huidige mobiliteitsbeleid?

**![datavisualisatie: Wouter Soudan :twitter: @rhythmvs](dataviz.jpg)**
