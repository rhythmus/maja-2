# De Inspiratie

Wat betekent inspiratie voor de moderne wetenschapper? In het boek _Daily Rituals. How Artists Work_ beschreef de Amerikaanse journalist Mason Currey de dagelijkse routines en rituelen van grote kunstenaars, denkers en wetenschappers. Wat hen bindt, zijn de vaak niet voor de hand liggende dagelijkse routines en rituelen. Ligt net daarin de bron voor hun inspiratie en creativiteit? 

❝ Wetenschappers moeten bovenal met rust gelaten worden. Zo borrelen dingen op. ![foto: Katleen Gabriels](JohanBraeckman.jpg)
